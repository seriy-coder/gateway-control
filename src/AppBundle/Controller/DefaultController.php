<?php

namespace AppBundle\Controller;

use AppBundle\Entity\DataLog;
use AppBundle\Entity\User;
use Faker\Generator;
use Faker\Provider\DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\SecurityBundle\Tests\Functional\Bundle\AclBundle\Entity\Car;
use Symfony\Component\HttpFoundation\Request;
use Carbon\Carbon;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', []);
    }

    /**
     * @Route("/companies", name="companies")
     */
    public function companiesListAction(){
        return $this->render('default/companies-list.html.twig');
    }

    /**
     * @Route("/users", name="users")
     */
    public function usersListAction(Request $request){
        $users = $this->getDoctrine()->getManager()->getRepository('AppBundle:User')->findAll();
        return $this->render('default/users-list.html.twig',[
                'users' => $users
            ]);
    }

    /**
     * @Route("/logs", name="traffic_logs")
     */
    public function logsListAction(Request $request){
        $generated = [];

        $user = new User();
        $user->setEmail('testuser@testmail.com');
        $user->setName('John Lennon');
        $user->setCompany(1);

        $startDate = (new Carbon('now'))->subMonth(6);
        $endDate = new Carbon('now');
        $year = 2016; // sets start year of generation

        $endDay = (new Carbon('now'));










        //
        //echo $startDate->month . '; ' . $endDate->month;
//        for( $month = $startDate->month; $month <= $endDate->month; $month++ ){
//
//            if ($month > 12) {
//                $month = 12 - $month;
//                $year++;
//            }
//
//            $endDay = $this->getDateObjByYm($year, $month);
//
//            for ($day = 1; $day<=30; $day++){
//                for ($row = 10; $row<=83; $row++){
//
//                }
//            }
//
//        }
//        $startMonth = $startDate->month;
//        $startMonth = ($startMonth > 12 ? 12 -  )
//        for ( $i= $startDate->month; $month<= )



//
//        for ($cnt = 3; $cnt <9; $cnt++){
//            for ($m=1; $m<=12; $m++){
//                $days = rand(0,3);
//                for ($d=0; $d<=$days; $d++){
//                    $record = new DataLog();
//                    $record->setBytess(rand(1, 10000000000)*100);
//                    //$date = new \DateTime('2016-0'.($cnt).'-'.$m.' '.rand(10,23).':'.rand(10,59));
//                    $date = new \DateTime();
//
//                    $date->setDate(2016, $m, rand(1, 30));
//                    $date->setTime(rand(1,23),rand(1,59));
//
//                    $record->setDate($date);
//                    $record->setUser($user);
//                    $res = \Faker\Factory::create();;
//                    $record->setResource($res->url());
//                    $generated[] = $record;
//                }
//            }
//        }

        $total = count($generated);

        return $this->render('default/logs-list.html.twig', [
            'total' => $total,
            'gdata' => $generated
        ]);
    }

    /**
     * @param $year
     * @param $month
     * @return Carbon
     */
    private function getDateObjByYm($year, $month){
        $strMonth = date("F",mktime(0, 0, 0, $month, 1, $year));
        $dt = new \DateTime('last day of ' . $strMonth . ' ' . $year); // <== instance from another API
        $carbon = Carbon::instance($dt);
        //echo get_class($carbon);                               // 'Carbon\Carbon'
        //echo $carbon->toDateTimeString();                      // 2008-01-01 00:00:00
        return $carbon;
    }


}
