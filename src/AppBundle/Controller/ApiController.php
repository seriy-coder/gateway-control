<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Entity\DataLog;
use AppBundle\Entity\User;
use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiController
 * @package AppBundle\Controller
 * @Route("/api", defaults={"_format": "json"}, options={"i18n" = false})
 */
class ApiController extends Controller
{
    /**
     * @Route("/companies", name="get_companies")
     * @Method("POST")
     * @return JsonResponse
     */
    public function getCompaniesListAction(){
        $retData = []; $retCount = 0;
        $em = $this->getDoctrine()->getManager();
        $companies = $em->getRepository('AppBundle:Company')->findAll();
        /** @var Company $company */
        foreach ($companies as $company){
            $retData[$retCount]['id'] = $company->getId();
            $retData[$retCount]['name'] = $company->getName();
            $retData[$retCount]['quota'] = $company->getQuota();
            $retCount++;
        }
        $retArr = ['success' => true, 'count' => $retCount, 'companies' => $retData];
        return new JsonResponse($retArr);
    }

    /**
     * @Route("/company/new", name="new_company")
     * @Method("POST")
     */
    public function createCompanyAction(Request $request){
        $params = $this->getParameters($request);
        $checkResult = $this->checkRequiredParams($params,['name*','quota*']);

        if ($checkResult === true){
            $em = $this->getDoctrine()->getManager();
            $company = new Company();
            $company->setName($params['name']);
            $company->setQuota($params['quota']);
            $em->persist($company);
            $em->flush();

            $retArr = ['success' => true, 'id' => $company->getId()];
        }else{
            $retArr = $checkResult;
        }
        return new JsonResponse($retArr);
    }
    /**
     * @Route("/company/edit", name="edit_company")
     * @Method("POST")
     */
    public function editCompanyAction(Request $request){
        $params = $this->getParameters($request);
        $checkResult = $this->checkRequiredParams($params,['id*','name*','quota*']);

        if ($checkResult === true){
            $em = $this->getDoctrine()->getManager();
            /** @var Company $company */
            $company = $em->getRepository('AppBundle:Company')->findOneBy(['id' => $params['id']]);

            if($company){
                $company->setName($params['name']);
                $company->setQuota($params['quota']);
                $em->persist($company);
                $em->flush();
            }else{
                return new JsonResponse(['success' => false, 'reason' => 'Company with id('.$params['id'].') does not exist.']);
            }

            $retArr = ['success' => true];
        }else{
            $retArr = $checkResult;
        }
        return new JsonResponse($retArr);
    }

    /**
     * @Route("/company/delete", name="delete_company")
     * @Method("POST")
     */
    public function deleteCompanyAction(Request $request){
        $params = $this->getParameters($request);
        $checkResult = $this->checkRequiredParams($params,['id*']);

        if ($checkResult === true){
            $em = $this->getDoctrine()->getManager();
            /** @var Company $company */
            $company = $em->getRepository('AppBundle:Company')->findOneBy(['id' => $params['id']]);
            if($company){
                $em->remove($company);
                $em->flush();
            }else{
                return new JsonResponse(['success' => false, 'reason' => 'Company with id('.$params['id'].') does not exist.']);
            }

            $retArr = ['success' => true];
        }else{
            $retArr = $checkResult;
        }
        return new JsonResponse($retArr);
    }

    /**
     * @Route("/users", name="get_users")
     * @Method("POST")
     */
    public function getUsersListAction(){
        $retData = []; $retCount = 0;
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('AppBundle:User')->findAll();
        /** @var User $user */
        foreach ($users as $user){
            $retData[$retCount]['id'] = $user->getId();
            $retData[$retCount]['name'] = $user->getName();
            $retData[$retCount]['email'] = $user->getEmail();
            $retData[$retCount]['companyName'] = $user->getCompany()->getName();
            $retCount++;
        }
        $retArr = ['success' => true, 'count'=> $retCount, 'users' => $retData];
        return new JsonResponse($retArr);
    }

    /**
     * @Route("/user/new", name="create_user")
     * @Method("POST")
     */
    public function createUserAction(Request $request)
    {
        $params = $this->getParameters($request);
        $checkResult = $this->checkRequiredParams($params,['name*','email*','company_id*']);

        if ($checkResult === true){
            $em = $this->getDoctrine()->getManager();
            /** @var Company $company */
            $company = $em->getRepository('AppBundle:Company')->findOneBy(['id' => $params['company_id']]);
            if($company){
                $user = new User();
                $user->setName($params['name']);
                $user->setCompany($company);
                $user->setEmail($params['email']);
                $em->persist($user);
                $em->flush();
            }else{
                return new JsonResponse(['success' => false, 'reason' => 'Company with id('.$params['company_id'].') does not exist.']);
            }


            $retArr = ['success' => true, 'id' => $user->getId()];
        }else{
            $retArr = $checkResult;
        }
        return new JsonResponse($retArr);
    }

    /**
     * @Route("/user/edit", name="edit_user")
     * @Method("POST")
     */
    // /{id}/{name}/{email}/{company_id}
    // requirements={"company_id": "\d+", "email": "^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$"}
    public function editUserAction(Request $request){

        $params = $this->getParameters($request);
        $checkResult = $this->checkRequiredParams($params,['id*','name*','email*','company_id*']);

        if ($checkResult === true){
            $em = $this->getDoctrine()->getManager();
            /** @var Company $company */
            $company = $em->getRepository('AppBundle:Company')->findOneBy(['id' => $params['company_id']]);
            if($company){
                /** @var User $user */
                $user = $em->getRepository('AppBundle:User')->findOneBy(['id' => $params['id']]);
                if($user){
                    $user->setName($params['name']);
                    $user->setCompany($company);
                    $user->setEmail($params['email']);
                    $em->persist($user);
                    $em->flush();
                }else{
                    return new JsonResponse(['success' => false, 'reason' => 'User with id('.$params['id'].') does not exist.']);
                }
            }else{
                return new JsonResponse(['success' => false, 'reason' => 'Company with id('.$params['company_id'].') does not exist.']);
            }

            $retArr = ['success' => true];
        }else{
            $retArr = $checkResult;
        }
        return new JsonResponse($retArr);
    }

    /**
     * @Route("/user/delete", name="delete_user")
     * @Method("POST")
     */
    public function deleteUserAction(Request $request){
        $params = $this->getParameters($request);
        $checkResult = $this->checkRequiredParams($params,['id*']);

        if ($checkResult === true){
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle:User')->findOneBy(['id' => $params['id']]);
            if($user){
                $em->remove($user);
                $em->flush();
            }else{
                return new JsonResponse(['success' => false, 'reason' => 'User with id('.$params['id'].') does not exist.']);
            }


            $retArr = ['success' => true];
        }else{
            $retArr = $checkResult;
        }
        return new JsonResponse($retArr);
    }

    /**
     * @Route("/logs", name="get_users_logs")
     * @Method("POST")
     */
    public function getLogsListAction(){
        $retData = []; $retCount = 0;
        $em = $this->getDoctrine()->getManager();
        $logs = $em->getRepository('AppBundle:DataLog')->findAll();
        /** @var DataLog $log */
        foreach ($logs as $log){

            $retData[$retCount]['name'] = $log->getUser()->getName();
            $retData[$retCount]['datetime'] = $log->getDate()->format('d M Y h:i:s');
            $retData[$retCount]['resource'] = $log->getResource();
            $retData[$retCount]['bytess'] = $this->formatSizeUnits( $log->getBytess() );
            $retCount++;
        }
        $retArr = ['success' => true, 'count'=> $retCount, 'logs' => $retData];
        return new JsonResponse($retArr);
    }
    /**
     * @Route("/ablogs", name="get_asusers_logs")
     * @Method("POST")
     */
    public function getAbusersLogsListAction(Request $request){
        $params = $this->getParameters($request);
        $checkResult = $this->checkRequiredParams($params,['date*']);
        $retData = []; $retCount = 0;

        if ($checkResult === true){
            $em = $this->getDoctrine()->getManager();
            $month = $params['date'];

            $Y = explode('/',$month)[1];
            $M = explode('/',$month)[0];

            $companies = $em->getRepository('AppBundle:Company')->findAll();
            /** @var Company $company */
            foreach ($companies as $company){
                $sql = "
                    SELECT SUM(data_log.bytess) as total, company.name, company.quota FROM data_log
                        LEFT join user 
                        ON user.id = data_log.user_id
                        LEFT join company 
                        ON company.id = user.company_id
                        WHERE data_log.date BETWEEN '".$Y.'-'.$M.'-01'."' AND '".$Y.'-'.$M.'-31'."' AND company.id=".$company->getId()."
                    ";

                $stmt = $em->getConnection()->prepare($sql);
                $stmt->execute();
                $row = $stmt->fetchAll()[0];

                if($row['total']>$row['quota']*1099511627776){
                    $retData[$retCount]['name'] = $row['name'];
                    $retData[$retCount]['used'] = $this->formatSizeUnits( $row['total'] );
                    $retData[$retCount]['quota'] = $row['quota'];
                    $retCount++;
                }
            }

            $retArr = ['success' => true,'count'=> $retCount, 'ablogs' => $retData];
        }else{
            $retArr = $checkResult;
        }
        return new JsonResponse($retArr);
    }

    /**
     * @Route("/service/generate", name="generate_fake_data")
     * @Method("POST")
     */
    public function generateDataAction(){
        $this->clearLogs();
        $this->initFakedUsers();

        $startDate = new Carbon('now');
        $endDate = (new Carbon('now'))->subMonth(6);
        $beforeMonth = $endDate->month;
        $monthIdx = 1; $oneActMonth = []; $retCount = 0;
        $maxMonthCount = 0;

        $maxmonth = rand(1,6);
        while($endDate->lt($startDate)){
            if ($maxmonth == $monthIdx){
                $this->addFakeUserLog($endDate);
            }elseif ($maxmonth != $monthIdx && !isset($oneActMonth[$monthIdx])){
                $this->addFakeUserLog($endDate);
                $retCount++;
                $oneActMonth[$monthIdx] = true;
            }

            $endDate->addDay(1);
            if ($beforeMonth != $endDate->month){
                $beforeMonth = $endDate->month;
                $monthIdx++;
            }
        }

        $retArr = ['success' => true, 'count'=> $retCount, 'mmc' => $maxMonthCount];
        return new JsonResponse($retArr);
    }

    private function clearLogs(){
        $em = $this->getDoctrine()->getManager();
        $cmd = $em->getClassMetadata('AppBundle:DataLog');
        $connection = $em->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $connection->query('SET FOREIGN_KEY_CHECKS=0');
        $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
        $connection->executeUpdate($q);
        $connection->query('SET FOREIGN_KEY_CHECKS=1');
    }

    private $fakedUsers;

    private function initFakedUsers(){
        $em = $this->getDoctrine()->getManager();
        /** @var User $users */
        $users = $em->getRepository('AppBundle:User')->findAll();
        foreach ($users as $user){
            $this->fakedUsers[$user->getId()]['activity'] = rand(2,14);
            $this->fakedUsers[$user->getId()]['obj'] = $user;
        }
    }

    private function addFakeUserLog(Carbon $carbonDate){
        $em = $this->getDoctrine()->getManager();
        foreach ($this->fakedUsers as $user){
            $user_obj = $user['obj'];

            for($i=0; $i<$user['activity']; $i++){
                $log = new DataLog();
                $log->setUser($user_obj);
                $date = new \DateTime($carbonDate->format('Y-m-d').' '.rand(1,23).':'.rand(1,59).':'.rand(1,59));
                $log->setDate($date);
                $log->setBytess(rand(1, 10000000000)*100);
                $fake = \Faker\Factory::create();
                $log->setResource($fake->url);
                $em->persist($log);
            }
        }


        $em->flush();
    }

    private function checkRequiredParams($checkArr, $needKeys){
        // check parameters exist
        if(count($checkArr) != count($needKeys)){
            $miss = [];
            foreach ($needKeys as $one){
                $flag = false;
                $one = str_ireplace('*', '', $one);
                foreach ($checkArr as $checkKey => $checkValue){
                    if ($checkKey == $one){
                        $flag = true;
                    }
                }
                if(!$flag){
                    $miss[] = $one;
                }
            }

            $params = ""; $counter = 0;
            foreach ($miss as $oneParam){
                $params .= "'".$oneParam . ( (count($miss) == $counter+1)?"' " : "', " );
                $counter++;
            }

            $retArr = ['success' => false, 'reason' => 'Required parameter(s) '.$params.' not found!'];
            return $retArr;
        }

        // validate parameters
        $paramCounter = 0; $checked = [];
        foreach ($checkArr as $key => $value){
            foreach ($needKeys as $checkKey){
                $origKey = $checkKey;
                $checkKey = str_ireplace('*','',$checkKey);
                if ($checkKey == $key && (! in_array($key, $checked))){

                    if (strpos($origKey, '*')===false) {
                    }else{
                        if (is_null($value) || $value == '') {
                            return ['success' => false, 'reason' => 'Parameter "'.$key.'" is required!'];
                        }
                    }

                    $checked[] = $key;
                    $paramCounter++;

                    switch ($checkKey){
                        case 'id':
                        case 'quota':
                        case 'company_id':
                            if (!is_numeric($value)){
                                $retArr = ['success' => false, 'reason' => 'Parameter "'.$key.'" required to be a number!'];
                                return $retArr;
                            }
                            break;
                        case 'email':
                            if(!$this->isValidEmail($value)){
                                $retArr = ['success' => false, 'reason' => 'Parameter "'.$key.'" required to be valid e-mail address!'];
                                return $retArr;
                            }
                            break;
                    }
                }
            }
        }

        return true;
    }

    private function isValidEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL)
        && preg_match('/@.+\./', $email);
    }

    private function isValidJSON($str) {
        json_decode($str);
        return json_last_error() == JSON_ERROR_NONE;
    }

    private function getParameters(Request $request){
        $content = urldecode( $request->getContent() );
        if (!empty($content))
        {
            if (!$this->isValidJSON($content)){
                $explString = explode('&', $content);
                $params = [];
                foreach ($explString as $one){
                    $subOne = explode('=', $one);
                    $params[$subOne[0]] = $subOne[1];
                }
                $content = json_encode($params);
            }
            if ($this->isValidJSON($content)){
                $params = json_decode( $content , true); // 2nd param to get as array
            }
            return $params;
        }else{
            return false;
        }
    }

    private function formatSizeUnits($bytes)
    {
        if ($bytes >= 1099511627776)
        {
            $bytes = number_format($bytes / 1099511627776) . ' TB';
        }
        elseif ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024) . ' kB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}
